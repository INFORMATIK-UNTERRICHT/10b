public class BedingteAnweisungen{
    // Attribute
    // Es werden keine Attribute in der Klasse benötigt
    // Attribute Ende
    //----------------------------------------
    // Konstruktoren
    public BedingteAnweisungen(){
        // Hier passiert gar nix
    }   
    // Konstruktoren Ende
    //----------------------------------------
    // Methoden
    public double aufgabe1(double rechnungssummePar){
        // Hier wird gegebenenfalls mit Kommazahlen gerechnet (d.h. mit Cent), 
        // daher benötigen wir einen neuen Datentyp. Statt int für Ganzzahlen
        // nun double für Kommazahlen.
        // Es wird zudem eine lokale Variable vom Datentyp double benötigt
        double endbetragLok = rechnungssummePar;
        if(rechnungssummePar < 300){
            // Hier steht der Quellcode der ausgeführt wird,
            // wenn die Bedingung erfüllt ist.
            // D.h. wenn die Rechnungsumme kleiner als 300 Euro beträgt
            endbetragLok = endbetragLok * 0.98;
            // Durch die Multiplikation mit 0,98 wird vom Betrag 2% abgezogen
            System.out.println("Der Endbetrag beträgt: " + endbetragLok);
            // Durch das return wird die Methode verlassen und alle lokalen Variablen
            // und Parameter gelöscht
            return endbetragLok;
        }
        else{
            // Hier steht der Quellcode der ausgeführt wird,
            // wenn die Bedingung NICHT erfüllt ist.
            // D.h. wenn die Rechnungsumme größer oder gleich 300 Euro beträgt
            endbetragLok = endbetragLok * 0.95;
            // Durch die Multiplikation mit 0,95 wird vom Betrag 5% abgezogen
            System.out.println("Der Endbetrag beträgt: " + endbetragLok);
            return endbetragLok;
        }
    }   
}