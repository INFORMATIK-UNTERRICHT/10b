class GehegeGiraffen{
    // Attribute
    private int anzahlEnthaltenerTiere; // Standartdatentyp
    private Giraffe erich; // Referenzattribut -> Objekt
    private Giraffe heino;
    private Giraffe greta;
    // Attribute Ende 
    // --------------------------------
    // Konstruktoren   
    public GehegeGiraffen(){
        this.heino = new Giraffe("männlich");
        this.erich = new Giraffe("männlich");
        this.greta = new Giraffe("weiblich");
        this.anzahlEnthaltenerTiere = 3;
    }
    // Konstruktoren Ende
    // --------------------------------
    // Methoden
    public void fuettern(){
        this.heino.fresse();
        this.erich.fresse();
        this.greta.fresse();       
    }
    
    public void informationenAusgeben(){
        this.heino.gibInformationenAus();
        this.erich.gibInformationenAus();
        this.greta.gibInformationenAus();
    }
    // Methoden Ende
}

