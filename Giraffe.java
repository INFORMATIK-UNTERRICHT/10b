public class Giraffe{
    // Attribute
    private int lebensenergie;
    private int lebensalter;
    private String geschlecht;
    private String fellfarbe;
    // Attribute Ende
    // ------------------------------
    // Konstruktoren 
    public Giraffe(){
        this.lebensenergie=75;
        this.lebensalter=0;
        this.fellfarbe="gelb";
        this.geschlecht = "männlich";
    }

    public Giraffe(String geschlechtPar){
        this.lebensenergie=75;
        this.lebensalter=0;
        this.fellfarbe="gelb";
        this.geschlecht = geschlechtPar;
    }
    // Konstruktoren Ende
    // Methoden
    public void setLebensenergie(int lebensenergiePar) {
        this.lebensenergie = lebensenergiePar;
    }

    public int getLebensenergie() {
        return lebensenergie;
    }

    public int getLebensalter() {
        return this.lebensalter;
    }

    public String getGeschlecht() {
        return this.geschlecht;
    }

    public String getFellfarbe() {
        return this.fellfarbe;
    }

    public void gibInformationenAus() {
        System.out.println("Lebensenergie: " + this.lebensenergie);
        System.err.println("Lebensalter: " + this.lebensalter);;
        System.out.println("Fellfarbe: " + this.fellfarbe);
        System.out.println("Geschlecht: " + this.geschlecht);
    }

    public void fresse() {
        this.setLebensenergie(lebensenergie + 5);
    } 
    // Methoden Ende
}