// Autor R. Löffler
// Datum 2021-01-19
// Projektname
public class Krokodil{
    // Attribute
    // Deklaration der Attribute
    private int lebensenergie;
    private int anzahlGefressenerTiere;
    private String hautfarbe;
    private String geschlecht;
    // Attribute Ende
    // -----------------------------
    // Konstruktoren -> Spezielle Methoden/ Einmalig ausgeführt
    // Ausführung EINES Konstruktors bei Erzeugung eines Objektes
    // Standardkonstruktor -> Da Parameterliste leer
    public Krokodil(){
        this.lebensenergie = 100;
        this.anzahlGefressenerTiere = 0;
        this.hautfarbe = "gelb" ;
        this.geschlecht = "männlich";
    } 
    // Nichtstandardkonstruktor -> Die Parameterliste NICHT leer
    public Krokodil(int lebensenergiePar){
        // Der Wert des Parameters lebensenergiePar wird in das
        // Attribut lebensenergie übertragen
        this.lebensenergie = lebensenergiePar;
        this.anzahlGefressenerTiere = 0;
        this.hautfarbe = "grün" ;
        this.geschlecht = "männlich";
        // Am Ende jeder Methode werden alle Parameter gelöscht
        // -> Lokale Speicher
        // Attribute hingegen sind globale Speicherpätze
        // -> Existieren auch nach Ende einer Methodenabarbeitung
    }
    // Nichtstandardkonstruktor -> Die Parameterliste NICHT leer
    public Krokodil(int lebensenergiePar,int anzahlGefressenerTierePar ){
        this.lebensenergie = lebensenergiePar;
        this.anzahlGefressenerTiere = anzahlGefressenerTierePar;
        this.hautfarbe = "rot" ;
    }

    public Krokodil(String geschlechtPar){
        this.lebensenergie = 100;
        this.anzahlGefressenerTiere = 0;
        this.hautfarbe = "grün" ;
        this.geschlecht = geschlechtPar;
    }
    // Konstruktoren Ende
    // -----------------------------
    // Methoden
    // Mit Rückgabe
    public int getLebensenergie(){
        return this.lebensenergie; // Datentyp von lebensenergie ist int
    }

    // Ohne Rückgabe
    public void setLebensenergie(int lebensenergiePar){
        this.lebensenergie = lebensenergiePar;
    }

    public void setAnzahlGefressenerTiere(int anzahlGefressenerTierePar) {
        this.anzahlGefressenerTiere = anzahlGefressenerTierePar;
    }

    public int getAnzahlGefressenerTiere() {
        return anzahlGefressenerTiere;
    }

    public String getHautfarbe() {
        return hautfarbe;
    }

    public void gibInformationenAus() {
        System.out.println("Lebensenergie: " + lebensenergie);
        System.err.println("Anzahl gefressene Tiere: " + anzahlGefressenerTiere);;
        System.out.println("Hautfarbe: " + hautfarbe);
    }

    public void fresse(){
        this.setLebensenergie(lebensenergie + 20);
        this.setAnzahlGefressenerTiere(anzahlGefressenerTiere + 1);
    } 
    // Methoden Ende
    // -----------------------------
}

